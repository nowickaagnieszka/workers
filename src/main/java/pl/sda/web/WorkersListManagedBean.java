package pl.sda.web;


import pl.sda.dao.WorkersDAO;
import pl.sda.dao.WorkersDAOHibertnateImpl;
import pl.sda.dto.Worker;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@SessionScoped
@ManagedBean(name = "workerList")
public class WorkersListManagedBean {

    private Worker newWorker = new Worker();
    private WorkersDAO workersDAO = new WorkersDAOHibertnateImpl();   // wczesniej WorkerDAOJDBCI
    private String filter;


    public List<Worker> getList() {
        if (filter != null && !filter.isEmpty()){
            return workersDAO.getWorkersByLastName(filter);
        }
        return workersDAO.getAllWorkers();
    }

    public void refresh(){
    }

        public void addNewWorker() {
        // dodać pracownika do bazy

        workersDAO.saveWorker(newWorker);
        newWorker = new Worker();

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Nowy pracownik został dodany!"));
    }

    public void deleteWorker(int id) {
        // usunać pracownika z bazy
        workersDAO.deleteWorker(id);

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Usunięto pracownika z id: " + id));
    }

    public void editWorker(int id) {
        //edycja pracownika w bazie
        newWorker= workersDAO.getWorker(id);
    }
    public void updateWorker(){
        //zapis zmian po edycji w bazie
        workersDAO.updateWorker(newWorker);
    }

    public Worker getNewWorker() {
        return newWorker;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter;
    }
}
