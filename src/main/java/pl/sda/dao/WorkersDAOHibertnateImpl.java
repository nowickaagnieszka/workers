package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.sda.dto.Worker;

import java.util.List;



public class WorkersDAOHibertnateImpl implements WorkersDAO{

    SessionFactory sessionFactory;

    public WorkersDAOHibertnateImpl() {
        this.sessionFactory = new Configuration().configure().buildSessionFactory(); //Configuration z hibernate
    }

    @Override
    public List<Worker> getAllWorkers() {
        Session session = sessionFactory.openSession();
       //  List<Worker> workers = session.createQuery("from Worker").list();
        List<Worker> workers = session.createCriteria(Worker.class).list();
        session.close();
        return workers;
    }

    @Override
    public List<Worker> getWorkersByLastName(String lastName) {
        return null;
    }

    @Override
    public Worker getWorker(int idW) {
        Session session = sessionFactory.openSession();
        Worker worker = session.get(Worker.class, idW);
        session.close();
        return worker;
    }

    @Override
    public void deleteWorker(int workerId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(getWorker(workerId));
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void saveWorker(Worker worker) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(worker);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void updateWorker(Worker worker) {
       this.saveWorker(worker);
    }
}
