package pl.sda.dao;

import pl.sda.dto.Worker;
import pl.sda.web.WorkersListManagedBean;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkersDAOJDBCI implements WorkersDAO{

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost/workers", "root", "gtbk743");
    }

    @Override
    public List<Worker> getWorkersByLastName(String lastName) {
        return null;
    }

    public List<Worker> getAllWorkers() {
        List<Worker> workers = new ArrayList<>();
        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement
                    ("SELECT * FROM workers");
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String position = rs.getString("position");
                int salary = rs.getInt("salary");
                int birthYear = rs.getInt("birthYear");
                Worker worker = new Worker();
                worker.setId(id);
                worker.setFirstName(firstName);
                worker.setLastName(lastName);
                worker.setPosition(position);
                worker.setSalary(salary);
                worker.setBirthYear(birthYear);
                workers.add(worker);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("wywaliło się na bazie");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }

        return workers;

    }
    public Worker getWorker(int idW) {
        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement
                    ("SELECT * FROM workers where id = ?");
            statement.setInt(1, idW);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String position = rs.getString("position");
                int salary = rs.getInt("salary");
                int birthYear = rs.getInt("birthYear");
                Worker worker = new Worker();
                worker.setId(id);
                worker.setFirstName(firstName);
                worker.setLastName(lastName);
                worker.setPosition(position);
                worker.setSalary(salary);
                worker.setBirthYear(birthYear);
                return worker;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("wywaliło się na bazie");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }

        return null;

    }
    public void deleteWorker(int workerId) {
        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement
                    ("delete from workers where id = ?");

            statement.setInt(1, workerId);
            statement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("wywaliło się na bazie");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }

    }
    public void saveWorker(Worker worker) {

        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement
                    ("INSERT INTO workers(firstName,lastName,position,salary,birthYear)VALUES (?,?,?,?,?) ");

            statement.setString(1, worker.getFirstName());
            statement.setString(2, worker.getLastName());
            statement.setString(3, worker.getPosition());
            statement.setInt(4, worker.getSalary());
            statement.setInt(5, worker.getBirthYear());
            statement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("wywaliło się na bazie");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }


    }
    public void updateWorker(Worker worker) {

        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement
                    ("UPDATE workers SET firstName= ?, lastName = ?, position = ?, salary = ?, birthYear =? WHERE id = ? ");

            statement.setString(1, worker.getFirstName());
            statement.setString(2, worker.getLastName());
            statement.setString(3, worker.getPosition());
            statement.setInt(4, worker.getSalary());
            statement.setInt(5, worker.getBirthYear());
            statement.setInt(6, worker.getId());
            statement.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("wywaliło się na bazie");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }

    }
}
