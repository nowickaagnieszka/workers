package pl.sda.dao;

import pl.sda.dto.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public interface WorkersDAO  {

    List<Worker> getAllWorkers();
    List<Worker> getWorkersByLastName(String lastName);

    Worker getWorker(int idW);

    void deleteWorker(int workerId);
    void saveWorker(Worker worker);
    void updateWorker(Worker worker);


}
